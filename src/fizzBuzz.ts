

const fizzBuzz = (inputNumber1: number, inputString1: string, inputNumber2: number, inputString2: string) => {

	// Generate an array of 100 numbers
	// than map over them to determine if the number for that entry should be replaced or not.

	const array = Array(100).fill(1).map((value, index) => {
		const currentNumber = index + 1;
		let replacementString = "";
		if (currentNumber % inputNumber1 === 0) {
			replacementString = inputString1;
		}

		if (currentNumber % inputNumber2 === 0) {
			replacementString += inputString2;
		}

		if (replacementString !== "") {
			return replacementString;
		}

		return currentNumber;
	});

	return array;
}

export default fizzBuzz;