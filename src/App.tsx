import React from 'react';
import './App.css';
import fizzBuzz from './fizzBuzz';


interface AppState {
  results?: Array<string | number>;
}


class App extends React.Component<{}, AppState> {
  inputNumber1: React.RefObject<HTMLInputElement>;
  inputString1: React.RefObject<HTMLInputElement>;
  inputNumber2: React.RefObject<HTMLInputElement>;
  inputString2: React.RefObject<HTMLInputElement>;
  constructor(props: any) {
    super(props);
    this.state = {};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.inputNumber1 = React.createRef();
    this.inputString1 = React.createRef();
    this.inputNumber2 = React.createRef();
    this.inputString2 = React.createRef();
  }

  handleSubmit(event: any) {
    // Compute list of numbers with substitutions.

    const results = fizzBuzz(
      parseInt(this.inputNumber1.current!.value),
      this.inputString1.current!.value,
      parseInt(this.inputNumber2.current!.value),
      this.inputString2.current!.value
    );

    this.setState({ results })
    event.preventDefault();
  }

  render() {
    return (<>
      <p className="hostingLink">
        The code is <a href="https://gitlab.com/mitchell.burton/naverisk-test">hosted on GitLab</a>.
      </p>

      <form onSubmit={this.handleSubmit}>
        <label>
          Number 1:
          <input type="number" ref={this.inputNumber1} />
        </label>
        <label>
          String 1:
          <input type="text" ref={this.inputString1} />
        </label>
        <label>
          Number 2:
          <input type="number" ref={this.inputNumber2} />
        </label>
        <label>
          String 2:
          <input type="text" ref={this.inputString2} />
        </label>
        <input type="submit" value="Submit" />
      </form>
      <div>
        {this.state.results?.map((entry, index) => <div key={index}><label>{index + 1}:</label>{entry}</div>)}
      </div>
    </>
    );
  }
}

export default App;
